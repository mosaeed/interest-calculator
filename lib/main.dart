import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Simple Interest Calculator App',
    home: SIForm(),
    theme: ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.indigo,
      accentColor: Colors.indigoAccent,
    ),
  ));
}

class SIForm extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _SIFormState();
  }
}

class _SIFormState extends State<SIForm> {
  var _currencies = ['Rupees', 'Dollars', 'Pounds'];
  final _minimumPadding = 5.0;
  TextStyle textStyle;
  var _currentSelectedItem = '';
  var displayResult = '';
  TextEditingController principalController = TextEditingController();
  TextEditingController roiController = TextEditingController();
  TextEditingController termController = TextEditingController();

  var _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _currentSelectedItem = _currencies[0];
  }

  @override
  Widget build(BuildContext context) {
    textStyle = Theme.of(context).textTheme.headline6;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Simple Interest Calculator App'),
      ),
      body: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(_minimumPadding * 2),
            child: ListView(
              children: [
                getImageAsset(),
                getPrincipalTextField(),
                getRateOfInterestTextField(),
                getRowOfTerm(),
                getRowOfButtons(),
                Padding(
                    padding: EdgeInsets.only(
                        top: _minimumPadding, bottom: _minimumPadding),
                    child: Text(
                      this.displayResult,
                      style: textStyle,
                    ))
              ],
            ),
          )),
    );
  }

  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/money.png');
    Image image = Image(
      image: assetImage,
      width: 125.0,
      height: 125.0,
    );

    return Container(
      child: image,
      margin: EdgeInsets.all(_minimumPadding * 10),
    );
  }

  Widget getPrincipalTextField() {
    return Padding(
        padding: EdgeInsets.only(top: _minimumPadding, bottom: _minimumPadding),
        child: TextFormField(
          style: textStyle,
          keyboardType: TextInputType.number,
          controller: principalController,
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'please enter principal amount';
            }

            return null;
          },
          decoration: InputDecoration(
              labelStyle: textStyle,
              labelText: 'Principal',
              hintText: 'Enter Principal e.g 12,000',
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
        ));
  }

  Widget getRateOfInterestTextField() {
    return Padding(
      padding: EdgeInsets.only(top: _minimumPadding, bottom: _minimumPadding),
      child: TextFormField(
        style: textStyle,
        keyboardType: TextInputType.number,
        controller: roiController,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'please enter Rate of Interest';
          }
          return null;
        },
        decoration: InputDecoration(
            labelStyle: textStyle,
            labelText: 'Rate of Interest',
            hintText: 'In Percent',
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
      ),
    );
  }

  Widget getRowOfTerm() {
    return Padding(
      padding: EdgeInsets.only(top: _minimumPadding, bottom: _minimumPadding),
      child: Row(
        children: [
          Expanded(
              child: TextFormField(
            style: textStyle,
            keyboardType: TextInputType.number,
            controller: termController,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'please enter Time in years';
              }
              return null;
            },
            decoration: InputDecoration(
                labelStyle: textStyle,
                labelText: "Term",
                hintText: "Time in years",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0))),
          )),
          Container(
            width: _minimumPadding * 5,
          ),
          Expanded(
              child: DropdownButton(
            items: _currencies.map((String valueSelected) {
              return DropdownMenuItem(
                child: Text(valueSelected),
                value: valueSelected,
              );
            }).toList(),
            value: _currentSelectedItem,
            onChanged: (String newValueSelected) {
              _onDropDownItemSelected(newValueSelected);
            },
          ))
        ],
      ),
    );
  }

  Widget getRowOfButtons() {
    return Padding(
      padding: EdgeInsets.only(top: _minimumPadding, bottom: _minimumPadding),
      child: Row(
        children: [
          Expanded(
              child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).accentColor),
                textStyle: MaterialStateProperty.all(
                    TextStyle(color: Theme.of(context).primaryColorLight))),
            child: Text(
              "Calculate",
              textScaleFactor: 1.5,
            ),
            onPressed: () {
              setState(() {
                if (_formKey.currentState.validate()) {
                  this.displayResult = _calculateTotalReturns();
                }
              });
            },
          )),
          Expanded(
              child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).primaryColorDark),
                textStyle: MaterialStateProperty.all(
                    TextStyle(color: Theme.of(context).primaryColorLight))),
            child: Text(
              "Reset",
              textScaleFactor: 1.5,
            ),
            onPressed: () {
              setState(() {
                _makeReset();
              });
            },
          ))
        ],
      ),
    );
  }

  void _onDropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentSelectedItem = newValueSelected;
    });
  }

  String _calculateTotalReturns() {
    double principal = double.parse(principalController.text);
    double roi = double.parse(roiController.text);
    double term = double.parse(termController.text);

    double totalAmountPayable = principal + (principal * roi * term) / 100;

    String result =
        'after $term years, your investment will be worth $totalAmountPayable $_currentSelectedItem';

    return result;
  }

  void _makeReset() {
    principalController.text = '';
    roiController.text = '';
    termController.text = '';
    displayResult = '';

    _currentSelectedItem = _currencies[0];
  }
}
